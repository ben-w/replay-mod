using Harmony;
using ReplayMod.Replayer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ReplayMod
{
    public class Main : VTOLMOD
    {
        public const string ReplayDataPath = "Replay Data.json";
        public const string ReplayFolder = "Replays";
        public const bool IsRecording = false;
        public const string DevReplayFolder = "23 - 13-46-51.6538459";

        private static Main _instance;
        private bool _recording;
        public override void ModLoaded()
        {
            base.ModLoaded();

            HarmonyInstance instance = HarmonyInstance.Create("marsh.replaymod");
            instance.PatchAll(Assembly.GetExecutingAssembly());

            VTOLAPI.SceneLoaded += SceneLoaded;
            _instance = this;
            Saving.ModFolder = ModFolder;
            if (!IsRecording)
                gameObject.AddComponent<Player>().ThisMod = this;
        }

        private void SceneLoaded(VTOLScenes arg0)
        {
            switch (arg0)
            {
                case VTOLScenes.OpenWater:
                case VTOLScenes.Akutan:
                case VTOLScenes.CustomMapBase:
                case VTOLScenes.CustomMapBase_OverCloud:
                    if (IsRecording)
                    {
                        Recorder.Start();
                        _recording = true;
                    }
                    break;
                default:
                    break;
            }
        }
        private void Update()
        {
            if (_recording)
                Recorder.Update();
        }
        public static void DebugLog(object message)
        {
            _instance.Log(message);
        }
        private void OnApplicationQuit()
        {
            Log("Application ending after " + Time.time + " seconds");
            _recording = false;
            Recorder.Stop();
        }

    }
}