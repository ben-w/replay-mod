﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valve.Newtonsoft.Json;

namespace ReplayMod
{
    static class Helper
    {
        public static bool TryDeserializeObject<T>(string json, out T deserilizedObject, out Exception error)
        {
            try
            {
                deserilizedObject = JsonConvert.DeserializeObject<T>(json);
                error = null;
                return true;
            }
            catch (Exception e)
            {
                error = e;
                deserilizedObject = default;
                return false;
            }
        }
    }
}
