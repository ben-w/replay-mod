﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using ReplayMod.Recording;
using ReplayMod.Classes;
using UnityEngine;

namespace ReplayMod
{
    static class Recorder
    {
        public const float SampleTime = 0.012f; // 0.012 is about 83 times per second
        private static float _currentTimer = 0.0f;
        // This class collects data
        // Then sends it to another thread
        // Where that one saves it
        private static List<Actor> _actorsList;
        private static ReplayData _replayData;

        private static Dictionary<int, AirActorData> _actors;
        public static void Update()
        {
            if (!Main.IsRecording)
                return;

            _currentTimer += Time.deltaTime;

            if (_currentTimer >= SampleTime)
            {
                Record();
                _currentTimer = 0;
            }
        }
        private static void Record()
        {
            _actorsList = TargetManager.instance.allActors;

            int lastID;
            for (int i = 0; i < _actorsList.Count; i++)
            {
                lastID = _actorsList[i].actorID;
                if (_actors.ContainsKey(lastID))
                {
                    SendData(_actors[lastID].UpdateData());
                }
                else
                {
                    _actors.Add(lastID, new AirActorData(_actorsList[i]));
                    _replayData.ReplayObjects.Add(new ReplayObject(lastID, $"{lastID}.json"));
                    SendData(_actors[lastID].UpdateData());
                }
            }
        }

        private static void SendData(Data data)
        {
            lock (Saving.DataQueue)
            {
                Saving.DataQueue.Enqueue(data);
            }
        }
        public static void Stop()
        {
            SendData(_replayData);
            Saving.Stop();
        }
        public static void Start()
        {
            ModFolder folder = new ModFolder() { Path = Saving.ModFolder };
            SendData(folder);

            _replayData = new ReplayData();
            _replayData.PilotName = PilotSaveManager.current.pilotName;
            _replayData.CampaignID = PilotSaveManager.currentCampaign.campaignID;
            _replayData.ScenarioID = PilotSaveManager.currentScenario.scenarioID;

            _actors = new Dictionary<int, AirActorData>();

            Saving.Start();
            Logger.Log("Starting Recording");
        }
    }
}
