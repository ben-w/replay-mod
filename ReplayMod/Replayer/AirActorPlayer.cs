﻿using ReplayMod.Classes;
using ReplayMod.Recording;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Harmony;

namespace ReplayMod.Replayer
{
    class AirActorPlayer : ActorPlayer
    {
        private AirActorData[] _replayData
        {
            get
            {
                return ReplayData as AirActorData[];
            }
        }
        private AirActorData _lastData
        {
            get
            {
                return LastData as AirActorData;
            }
        }

        private Actor _thisActor;
        private AutoPilot _autoPilot;
        private AeroController _aeroController;
        private WheelsController _wheelsController;
        private ModuleEngine[] _moduleEngines;

        private Traverse _overrideRollTarget;
        private Traverse[] _engineThrusts;

        private Vector3 _lastRollTarget = new Vector3();

        public void ApplyBaseVariables()
        {
            _thisActor = GetComponent<Actor>();
            _autoPilot = GetComponent<AutoPilot>();
            _aeroController = GetComponent<AeroController>();
            _wheelsController = GetComponent<WheelsController>();
            _moduleEngines = GetComponentsInChildren<ModuleEngine>();
            _engineThrusts = new Traverse[_moduleEngines.Length];

            if (_thisActor == null)
                Logger.LogError("_thisActor is null");
            if (_autoPilot == null)
                Logger.LogError("_autoPilot is null");

            _thisActor.actorName = _replayData[0].ActorName;
            GetTraverses();
            Player.PlaySample += base.PlaybackLoop;
        }
        private void GetTraverses()
        {
            Traverse autoPilot = Traverse.Create(_autoPilot);
            _overrideRollTarget = autoPilot.Field("overrideRollTarget");

            for (int i = 0; i < _moduleEngines.Length; i++)
            {
                _engineThrusts[i] = Traverse.Create(_moduleEngines[i]).Field("throttle");
                _moduleEngines[i].autoAB = true;
            }
        }

        private void Update()
        {
            if (_aeroController != null)
            {
                _aeroController.input = _lastRollTarget;
                _aeroController.brake = _lastData.Breaks;
                _aeroController.flaps = _lastData.Flaps;
            }
            if (_moduleEngines != null)
            {
                for (int i = 0; i < _engineThrusts.Length; i++)
                {
                    _engineThrusts[i].SetValue(_lastData.Throttle[i]);
                }
            }
        }
        public override void PlayData(int position)
        {
            base.PlayData(position);

            _lastRollTarget.x = _lastData.Pitch;
            _lastRollTarget.y = _lastData.Yaw;
            _lastRollTarget.z = _lastData.Roll;
        }

        private void OnDisable()
        {
            Player.PlaySample -= base.PlaybackLoop;
        }
    }
}
