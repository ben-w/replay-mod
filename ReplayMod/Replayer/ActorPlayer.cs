﻿using ReplayMod.Classes;
using ReplayMod.Recording;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Harmony;

namespace ReplayMod.Replayer
{
    class ActorPlayer : MonoBehaviour
    {
        public ActorData[] ReplayData;
        public ActorData LastData;
        public int CurrentPosition { get; private set; } = 0;
        public virtual void PlaybackLoop()
        {
            if (ReplayData == null ||
                   ReplayData.Length <= CurrentPosition + 1)
            {
                return;
            }
            PlayData(CurrentPosition);
            CurrentPosition++;
        }
        public virtual void PlayData(int position)
        {
            LastData = ReplayData[position];
            transform.position = VTMapManager.GlobalToWorldPoint(LastData.WorldPosition.Vector3D);
            transform.rotation = Quaternion.Euler(LastData.Rotation.Vector3);
        }
    }
}
