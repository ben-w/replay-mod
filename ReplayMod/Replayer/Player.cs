﻿// This class displays the data from the Loading class
using ReplayMod.Classes;
using ReplayMod.Recording;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ReplayMod.Replayer
{
    class Player : MonoBehaviour
    {
        public static Action PlaySample;
        private ReplayData _replay;
        public Main ThisMod;
        private float _currentTime = 0.0f;
        private void Start()
        {
            Logger.Log("Player Start");
            _replay = Loading.LoadReplay(Path.Combine(ThisMod.ModFolder, Main.ReplayFolder, Main.DevReplayFolder));
            LoadReplayer();
            VTOLAPI.SceneLoaded += SceneLoaded;
        }

        private void SceneLoaded(VTOLScenes arg0)
        {
            if (arg0 == VTOLScenes.Akutan)
                AddPlayers();
        }
        private void Update()
        {
            _currentTime += Time.deltaTime;
            if (_currentTime >= Recorder.SampleTime)
            {
                PlaySample?.Invoke();
                _currentTime = 0f;
            }
        }

        private void LoadReplayer()
        {
            StartCoroutine(LoadScenario());
        }
        private IEnumerator LoadScenario()
        {
            Logger.Log("Loading Scenario");
            VTMapManager.nextLaunchMode = VTMapManager.MapLaunchModes.Scenario;

            Logger.Log("Setting Pilot");
            yield return LoadPilot(_replay.PilotName);

            Logger.Log("Setting Scenario and Campaign");
            SetScenario(_replay.CampaignID, _replay.ScenarioID);

            Logger.Log("Launching");
            VTScenario.LaunchScenario(VTScenario.currentScenarioInfo, true);
        }
        private IEnumerator LoadPilot(string pilotName)
        {
            if (PilotSaveManager.pilots == null ||
                PilotSaveManager.pilots.Count == 0)
            {
                PilotSaveManager.LoadPilotsFromFile();

                while (PilotSaveManager.pilots == null ||
                    PilotSaveManager.pilots.Count == 0)
                {
                    yield return null;
                }
            }

            if (!PilotSaveManager.pilots.ContainsKey(pilotName))
            {
                Logger.LogError($"Pilot {pilotName} wasn't found");
                yield break;
            }
            PilotSaveManager.current = PilotSaveManager.pilots[pilotName];
        }
        private void SetScenario(string campaignID, string scenarioID)
        {
            List<VTCampaignInfo> campaigns = VTResources.GetBuiltInCampaigns();
            if (campaigns != null)
            {
                foreach (VTCampaignInfo info in campaigns)
                {
                    if (info.campaignID == campaignID)
                    {
                        Logger.Log("Set Campaign");
                        PilotSaveManager.currentCampaign = info.ToIngameCampaign();
                        PilotSaveManager.currentVehicle = VTResources.GetPlayerVehicle(info.vehicle);
                        break;
                    }
                }
            }
            else
                Logger.LogError("Campaigns are null");


            foreach (CampaignScenario cs in PilotSaveManager.currentCampaign.missions)
            {
                if (cs.scenarioID == scenarioID)
                {
                    Logger.Log("Set Scenario");
                    PilotSaveManager.currentScenario = cs;
                    break;
                }
            }

            VTScenario.currentScenarioInfo = VTResources.GetScenario(PilotSaveManager.currentScenario.scenarioID, PilotSaveManager.currentCampaign);
        }
        private void AddPlayers()
        {
            List<Actor> actors = TargetManager.instance.allActors;
            for (int i = 0; i < actors.Count; i++)
            {
                for (int j = 0; j < _replay.ReplayObjects.Count; j++)
                {
                    if (actors[i].actorID == _replay.ReplayObjects[j].ObjectID)
                    {
                        AirActorPlayer player = actors[i].gameObject.AddComponent<AirActorPlayer>();
                        player.ReplayData = Loading.GetActorData(Path.Combine(ThisMod.ModFolder, Main.ReplayFolder, Main.DevReplayFolder, _replay.ReplayObjects[j].ReplayFile));
                        player.ApplyBaseVariables();
                        break;
                    }
                }
            }
        }
    }
}
