﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Harmony;

namespace ReplayMod.Patches
{
    [HarmonyPatch(typeof(ModuleEngine), "SetThrottle")]
    class ModuleEngine_SetThrottle
    {
        [HarmonyPrefix]
        public static bool Prefix(ModuleEngine __instance, float t)
        {
            if (!Main.IsRecording)
            {
                Logger.Log("Stopped ModuleEngine SetThrottle Overide");
                return false;
            }
            return true;
        }
    }
}
