﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Harmony;
using UnityEngine;

namespace ReplayMod
{
    [HarmonyPatch(typeof(AeroController), "SetPitchYawRoll")]
    class AeroController_SetPitchYawRoll
    {
        [HarmonyPrefix]
        public static bool Prefix(AeroController __instance, Vector3 pitchYawRoll)
        {
            if (!Main.IsRecording)
            {
                Logger.Log("Stopped AeroController SetPitchYawRoll Overide");
                return false;
            }
            return true;
        }
    }
}
