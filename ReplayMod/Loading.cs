﻿// This class just loads the data in. Not displaying it.
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReplayMod.Classes;
using ReplayMod.Recording;

namespace ReplayMod
{
    static class Loading
    {
        private static DirectoryInfo _replayDirectory;
        public static ReplayData LoadReplay(string folder)
        {
            _replayDirectory = new DirectoryInfo(folder);
            return GetReplayData();
        }
        private static ReplayData GetReplayData()
        {
            bool loaded = Helper.TryDeserializeObject<ReplayData>(
                File.ReadAllText(Path.Combine(_replayDirectory.FullName, Main.ReplayDataPath)),
                out ReplayData replayData,
                out Exception error);

            if (!loaded)
            {
                Logger.LogError("Failed to load replay data!\n" + error.Message);
                return null;
            }
            Logger.Log("Loaded replay data");
            return replayData;
        }
        public static AirActorData[] GetActorData(string file)
        {
            bool loaded = Helper.TryDeserializeObject<AirActorData[]>(
                File.ReadAllText(file),
                out AirActorData[] array,
                out Exception error);
            if (!loaded)
            {
                Logger.LogError($"Failed to load the actor data at {file}\n{error}");
                return null;
            }
            return array;
        }
    }
}
