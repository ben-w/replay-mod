﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Valve.Newtonsoft.Json;

namespace ReplayMod.Classes
{
    struct Vector3J
    {
        [JsonProperty("X")]
        public float X;
        [JsonProperty("Y")]
        public float Y;
        [JsonProperty("Z")]
        public float Z;
        [JsonIgnore]
        public Vector3 Vector3
        {
            get
            {
                return new Vector3(X, Y, Z);
            }
        }
        [JsonIgnore]
        public Vector3D Vector3D
        {
            get
            {
                return new Vector3D(X, Y, Z);
            }
        }

        public Vector3J(Vector3 vector)
        {
            X = vector.x;
            Y = vector.y;
            Z = vector.z;
        }
    }
}
