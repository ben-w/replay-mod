﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ReplayMod.Classes;
using ReplayMod.Recording;
using Valve.Newtonsoft.Json;

namespace ReplayMod
{
    static class Saving
    {
        public static Queue<Data> DataQueue = new Queue<Data>();
        public static string ModFolder;
        public static string CurrentReplayFolder;


        private static DateTime _start;
        private static ReplayData _replayData;
        private static object _locker = new object();
        private static bool _missionLocker = false;
        private static Thread _savingThread;
        private static Data _lastData;
        public static bool InMission
        {
            get
            {
                lock (_locker)
                {
                    return _missionLocker;
                }
            }
            set
            {
                lock (_locker)
                {
                    _missionLocker = value;
                }
            }
        }

        public static void Start()
        {
            InMission = true;
            _savingThread = new Thread(new ThreadStart(Thread));
            _savingThread.IsBackground = true;
            _savingThread.Start();
            Log("Started");
        }
        public static void Stop()
        {
            //InMission = false;
            Log("Finished");
        }
        private static void Thread()
        {
            Log("Started Thread");
            _start = DateTime.Now;
            while (InMission)
            {
                lock (DataQueue)
                {
                    if (DataQueue.Count == 0)
                        continue;
                    _lastData = DataQueue.Dequeue();
                }
                Log($"Received {_lastData.DataType} from main thread");
                switch (_lastData.DataType)
                {
                    case Data.Type.ReplayData:
                        _replayData = (ReplayData)_lastData;
                        WriteReplayData();
                        InMission = false;
                        continue;
                    case Data.Type.ModFolder:
                        ModFolder = ((ModFolder)_lastData).Path;
                        CreateReplayFolder();
                        continue;
                    case Data.Type.ActorData:
                        SaveActorData(_lastData as AirActorData);
                        break;
                    default:
                        break;
                }
            }
            FinishAllFiles();
            Log("Should be saved now");
        }
        private static void WriteReplayData()
        {
            string path = Path.Combine(CurrentReplayFolder, Main.ReplayDataPath);
            using (StreamWriter file = File.CreateText(path))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Formatting = Formatting.Indented;
                serializer.Serialize(file, _replayData);
            }
            Log("Saved replay data to " + path);
        }
        private static void CreateReplayFolder()
        {
            CurrentReplayFolder = Path.Combine(ModFolder, Main.ReplayFolder, GetFolderName(_start));
            Directory.CreateDirectory(CurrentReplayFolder);
        }
        private static string GetFolderName(DateTime time)
        {
            return $"{time.DayOfYear} - {time.TimeOfDay.ToString().Replace(':', '-')}";
        }
        private static void SaveActorData(AirActorData data)
        {
            string path = Path.Combine(CurrentReplayFolder, $"{data.ActorID}.json");
            bool fileExists = File.Exists(path);
            using (StreamWriter file = new StreamWriter(path, true))
            {
                if (!fileExists)
                {
                    file.Write("[");
                }
                else
                {
                    file.Write(",");
                }

                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(file, data);
            }
        }
        private static void FinishAllFiles()
        {
            DirectoryInfo replayFolder = new DirectoryInfo(CurrentReplayFolder);
            FileInfo[] files = replayFolder.GetFiles("*.json");

            for (int i = 0; i < files.Length; i++)
            {
                if (files[i].Name == Main.ReplayDataPath)
                    continue;
                CloseActorFile(files[i].FullName);
            }
        }
        private static void CloseActorFile(string path)
        {
            Log("Closing json in " + path);
            using (StreamWriter file = new StreamWriter(path, true))
            {
                file.Write("]");
            }
        }
        private static void Log(string message)
        {
            // Thread Safe
            UnityEngine.Debug.Log("[Saving.cs] " + message);
        }
    }
}
