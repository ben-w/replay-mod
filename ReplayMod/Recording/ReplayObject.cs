﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valve.Newtonsoft.Json;

namespace ReplayMod.Recording
{
    [JsonObject(MemberSerialization.OptIn)]
    class ReplayObject
    {
        [JsonProperty("Object ID")]
        public int ObjectID;
        [JsonProperty("Replay File")]
        public string ReplayFile;

        public ReplayObject(int objectID, string replayFile)
        {
            ObjectID = objectID;
            ReplayFile = replayFile;
        }
    }
}
