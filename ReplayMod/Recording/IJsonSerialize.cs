﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valve.Newtonsoft.Json;

namespace ReplayMod.Recording
{
    interface IJsonSerialize
    {
        void GetJsonString(JsonTextWriter jsonWriter);
    }
}
