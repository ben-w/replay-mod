﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Valve.Newtonsoft.Json;

namespace ReplayMod.Recording
{
    [JsonObject(MemberSerialization.OptIn)]
    class AirActorData : ActorData
    {
        [JsonProperty("Flaps")]
        public float Flaps;
        [JsonProperty("Pitch")]
        public float Pitch;
        [JsonProperty("Yaw")]
        public float Yaw;
        [JsonProperty("Roll")]
        public float Roll;
        [JsonProperty("Breaks")]
        public float Breaks;
        [JsonProperty("Landing Gear State")]
        public bool LandingGearState;
        [JsonProperty("Throttle")]
        public float[] Throttle;

        private AeroController _aeroController;
        private WheelsController _wheelsController;
        private ModuleEngine[] _moduleEngines;

        public AirActorData(Actor actor) : base(actor)
        {
            if (actor == null)
                return;
            _aeroController = actor.GetComponent<AeroController>();
            _wheelsController = actor.GetComponent<WheelsController>();
            _moduleEngines = actor.GetComponentsInChildren<ModuleEngine>();
            Throttle = new float[_moduleEngines.Length];
        }
        public new AirActorData UpdateData()
        {
            base.UpdateData();
            Flaps = _aeroController.flaps;
            Pitch = _aeroController.input.x;
            Yaw = _aeroController.input.y;
            Roll = _aeroController.input.z;
            Breaks = _aeroController.brake;

            for (int i = 0; i < _moduleEngines.Length; i++)
            {
                Throttle[i] = _moduleEngines[i].inputThrottle;
            }

            LandingGearState = GetLandingGearState();
            return this;
        }
        private bool GetLandingGearState()
        {
            if (_wheelsController != null)
            {
                return _wheelsController.gearAnimator.GetCurrentState() == GearAnimator.GearStates.Extended;
            }
            return false;
        }
    }
}
