﻿using ReplayMod.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Valve.Newtonsoft.Json;

namespace ReplayMod.Recording
{
    [JsonObject(MemberSerialization.OptIn)]
    class ActorData : Data
    {
        [JsonProperty("Actor ID")]
        public int ActorID;
        [JsonProperty("Actor Name")]
        public string ActorName;
        [JsonProperty("Role")]
        public Actor.Roles Role;
        [JsonProperty("World Position")]
        public Vector3J WorldPosition;
        [JsonProperty("Rotation")]
        public Vector3J Rotation;

        public Actor ActorObject;

        private Transform _transform;
        [JsonIgnore]
        private Vector3D _lastGlobalPoint;

        public ActorData(Actor actor)
        {
            if (actor == null)
                return;
            Role = actor.role;
            ActorName = actor.actorName;
            ActorID = actor.actorID;
            DataType = Type.ActorData;
            ActorObject = actor;
            _transform = actor.transform;
        }

        public ActorData UpdateData()
        {
            _lastGlobalPoint = VTMapManager.WorldToGlobalPoint(ActorObject.position);
            WorldPosition.X = (float)_lastGlobalPoint.x;
            WorldPosition.Y = (float)_lastGlobalPoint.y;
            WorldPosition.Z = (float)_lastGlobalPoint.z;
            Rotation.X = _transform.rotation.eulerAngles.x;
            Rotation.Y = _transform.rotation.eulerAngles.y;
            Rotation.Z = _transform.rotation.eulerAngles.z;
            return this;
        }
    }
}
