﻿using ReplayMod.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valve.Newtonsoft.Json;

namespace ReplayMod.Recording
{
    [JsonObject(MemberSerialization.OptIn)]
    class ReplayData : Data
    {
        [JsonProperty("Campaign ID")]
        public string CampaignID;
        [JsonProperty("Scenario ID")]
        public string ScenarioID;
        [JsonProperty("Pilot Name")]
        public string PilotName;

        [JsonProperty("Replay Objects")]
        public List<ReplayObject> ReplayObjects = new List<ReplayObject>();

        public ReplayData()
        {
            DataType = Type.ReplayData;
        }
    }
}
