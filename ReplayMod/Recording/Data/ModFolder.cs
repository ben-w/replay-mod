﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplayMod.Recording
{
    class ModFolder : Data
    {
        public string Path;

        public ModFolder()
        {
            DataType = Type.ModFolder;
        }
    }
}
