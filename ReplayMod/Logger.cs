﻿// This class is for thread safe logging
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplayMod
{
    static class Logger
    {
        public static void Log(object message)
        {
            UnityEngine.Debug.Log("[Replay Mod] " + message);
        }
        public static void LogWarning(object message)
        {
            UnityEngine.Debug.LogWarning("[Replay Mod] " + message);
        }
        public static void LogError(object message)
        {
            UnityEngine.Debug.LogError("[Replay Mod] " + message);
        }
    }
}
